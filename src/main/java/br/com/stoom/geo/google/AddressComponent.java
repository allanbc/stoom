package br.com.stoom.geo.google;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressComponent {
   
   @JsonProperty("long_name")
   String longName;
   
   @JsonProperty("short_name")
   String shortName;
   
   List<String> types;

}
