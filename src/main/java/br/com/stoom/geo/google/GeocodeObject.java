package br.com.stoom.geo.google;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeocodeObject {
   @JsonProperty("place_id")
   String placeId;
   
   @JsonProperty("address_components")
   List<AddressComponent> addressComponents;
   
   @JsonProperty("formatted_address")
   String formattedAddress;
   GeocodeGeometry geometry;
   
}
