package br.com.stoom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.stoom.modelo.GeoDomain;

@Repository
public interface GeoRepository extends JpaRepository<GeoDomain, Long> {

}
