package br.com.stoom.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.stoom.controller.dto.GeoFilter;
import br.com.stoom.modelo.GeoDomain;

public interface AdvancedGeoSearch {
	
	Page<GeoDomain> findAll(GeoFilter filter, Pageable pageable);
}
