package br.com.stoom.controller.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.stoom.modelo.GeoDomain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeoRequest {
	
	private Long id;
	
	@NotEmpty
	private String streetName;
	
	@NotNull
	private Integer number;
	
	@NotEmpty
	private String complement;
	
	@NotEmpty
	private String neighbourhood;
	
	@NotEmpty
	private String city;
	
	@NotEmpty
	private String state;
	
	@NotEmpty
	private String country;
	
	@NotEmpty
	private String zipcode;
	
	private String latitude;
	
	private String longitude;
	
	public GeoDomain requestDomainBuilder() {
		return GeoDomain.builder()
				.streetName(streetName)
				.number(number)
				.complement(complement)
				.neighbourhood(neighbourhood)
				.city(city)
				.state(state)
				.country(country)
				.zipcode(zipcode)
				.latitude(latitude)
				.longitude(longitude)
				.build();
	}
	
	@Override
	public String toString() {
		return  streetName + "+" + neighbourhood + "+" + city
				+ "+" + state + "+" + country + "+" + zipcode;
	}

}
