package br.com.stoom.controller.dto;

import java.util.List;

import br.com.stoom.modelo.GeoDomain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeoResponse {
	
	private Long id;
	
	private List<GeoDomain> geoDomainList;
	
	private Long total;
	
	private String streetName;
	
	private Integer number;
	
	private String complement;
	
	private String neighbourhood;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String zipcode;
	
	private String latitude;
	
	private String longitude;
	
}
