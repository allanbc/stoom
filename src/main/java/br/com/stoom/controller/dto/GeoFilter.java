package br.com.stoom.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeoFilter {
	
	private String streetName;
	
	private String neighbourhood;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String zipcode;
	
	private String latitude;
	
	private String longitude;
		
}
