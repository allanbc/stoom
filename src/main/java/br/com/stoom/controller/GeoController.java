package br.com.stoom.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.stoom.controller.dto.GeoRequest;
import br.com.stoom.geo.google.GeocodeResult;
import br.com.stoom.modelo.GeoDomain;
import br.com.stoom.service.GeoService;
import br.com.stoom.utils.RemoveAcentosUtil;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;

@RestController
@RequestMapping("/geo")
@RequiredArgsConstructor
@Validated
public class GeoController {
	
	private final GeoService service;
	
	@GetMapping
	public ResponseEntity<List<GeoDomain>> findAll(
			@PageableDefault(sort = "city", direction = Direction.DESC, page = 0, size = 10) Pageable paginacao) {
		return ResponseEntity.ok(service.findAll(paginacao));
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		
		return ResponseEntity.status(HttpStatus.OK).body(service.findById(id));
	}
	
	@PostMapping
	@Transactional
	public ResponseEntity<GeoRequest> create(@RequestBody @Valid GeoRequest request) throws IOException {
		
		GeocodeResult result = verificaGeoCode(request);
		
		request.setLatitude(result.getResults().get(0).getGeometry().getGeocodeLocation().getLatitude());
		request.setLongitude(result.getResults().get(0).getGeometry().getGeocodeLocation().getLongitude());
		
		GeoDomain gd = service.create(request);
		
		return ResponseEntity.status(HttpStatus.CREATED).header("id", gd.getId().toString()).build();
	}
	
	private GeocodeResult verificaGeoCode(@Valid GeoRequest request) throws IOException {
		String endereco = StringUtils.EMPTY;
		if(StringUtils.isEmpty(request.getLatitude()) && (StringUtils.isEmpty(request.getLongitude()))) {
			
			endereco = request.toString().replace(" ", "+");
			
			endereco = RemoveAcentosUtil.removerAcentos(endereco);
			
		}
		return getGeocode(endereco);
		
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<GeoRequest> update(@PathVariable Long id, @RequestBody @Valid GeoRequest requestPut) {
		
		service.update(id, requestPut);
		
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		service.delete(id);
		
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/geocode")
	public GeocodeResult getGeocode(@RequestParam String address) throws IOException {
		
		OkHttpClient client = new OkHttpClient();
		String encodedAddress = URLEncoder.encode(address, "UTF-8");
		String auth = encodedAddress.concat("&key=AIzaSyCj0cY2yEvVfYhAaTz3-P2MW-YRKmhz5Uw");
		Request request = new Request.Builder()
				.url("https://maps.googleapis.com/maps/api/geocode/json?address="+auth)
				.get()
				.addHeader("host", "maps.googleapis.com")
				.addHeader("key", auth)
				.build();
		
		ResponseBody responseBody = client.newCall(request).execute().body();
		
		ObjectMapper objectMapper = new ObjectMapper();
		GeocodeResult result = objectMapper.readValue(responseBody.string(), GeocodeResult.class);
		
		return result;
	}
	
}