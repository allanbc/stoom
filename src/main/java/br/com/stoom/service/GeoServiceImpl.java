package br.com.stoom.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.stoom.controller.dto.GeoRequest;
import br.com.stoom.modelo.GeoDomain;
import br.com.stoom.repository.GeoRepository;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class GeoServiceImpl implements GeoService {
	
	private final GeoRepository repository;
	
	@Override
	public GeoDomain create(GeoRequest request) {
		return repository.save(request.requestDomainBuilder());
	}

	@Override
	public void delete(Long id) {
		Optional<GeoDomain> optional = repository.findById(id);
		if (optional.isPresent()) {
			repository.deleteById(id);
		}
	}

	@Override
	public GeoDomain update(Long id, GeoRequest requestPut) {
		
		GeoDomain geoDomain = this.findById(id);
		BeanUtils.copyProperties(requestPut, geoDomain, "id");
		return repository.save(geoDomain);
	}

	@Override
	public GeoDomain findById(Long id) {
		
		try {
			return repository.findById(id)
					.orElseThrow(()-> new NotFoundException("Geo Error"));
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<GeoDomain> findAll(Pageable paginacao) {
		return repository.findAll();
	}

}
