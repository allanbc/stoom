package br.com.stoom.service;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.stoom.controller.dto.GeoRequest;
import br.com.stoom.modelo.GeoDomain;

@Service
public interface GeoService {
	  
	GeoDomain create(GeoRequest request);

	void delete(Long id);

	GeoDomain update(Long id, GeoRequest requestPut);

	GeoDomain findById(Long id);

	List<GeoDomain> findAll(Pageable paginacao);
}
