package br.com.stoom.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.stoom.controller.dto.GeoResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper=false)
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="geo")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeoDomain {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_geo")
	private Long id;
	
	@Column(name="street_name")
	private String streetName;
	
	@Column(name="number_add")
	private Integer number;
	
	@Column(name="complement")
	private String complement;
	
	@Column(name="neighbourhood")
	private String neighbourhood;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="country")
	private String country;
	
	@Column(name="zipcode")
	private String zipcode;
	
	@Column(name="latitude")
	private String latitude;
	
	@Column(name="longitude")
	private String longitude;
	
	public GeoResponse toGeoResponse() {
		return GeoResponse.builder()
				.streetName(this.streetName)
				.number(this.number)
				.complement(this.complement)
				.neighbourhood(this.neighbourhood)
				.city(this.city)
				.state(this.state)
				.country(this.country)
				.zipcode(this.zipcode)
				.latitude(this.latitude)
				.longitude(this.longitude)
				.build();
	}

}
