
INSERT INTO USUARIO(nome, email, senha) VALUES('stoom', 'prova-stoom@email.com', '$2a$10$sFKmbxbG4ryhwPNx/l3pgOJSt.fW1z6YcUnuE2X8APA/Z3NI/oSpq');

INSERT INTO GEO(street_name, number_add, complement, neighbourhood, city, state, country, zipcode, latitude, longitude) 
VALUES('Rua Paes de Andrade', '234', 'L16 Q35', 'Olavo Bilac', 'Duque de Caxias', 'RJ', 'Brazil', '25036240', '22.7617399', '-43.3186423');

INSERT INTO GEO(street_name, number_add, complement, neighbourhood, city, state, country, zipcode, latitude, longitude) 
VALUES('Rua Brás Cubas', '11', 'SN', 'Olavo Bilac', 'Duque de Caxias', 'RJ', 'Brazil', '25036240', '22.7617399', '-43.3186423');
