package br.com.stoom.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;

import br.com.stoom.controller.dto.GeoFilter;
import br.com.stoom.controller.dto.GeoRequest;
import br.com.stoom.controller.dto.GeoResponse;
import br.com.stoom.modelo.GeoDomain;
import br.com.stoom.repository.GeoRepository;
import br.com.stoom.service.GeoServiceImpl;

@ExtendWith(MockitoExtension.class)
public class GeoServiceImplTest {
	
	private static Long id = 1L;
	
	private static String streetName;
	
	private static Integer number;
	
	private static String complement;
	
	private static String neighbourhood;
	
	private static String city;
	
	private static String state;
	
	private static String country;
	
	private static String zipcode;
	
	private static String latitude;
	
	private static String longitude;
	
	@InjectMocks
	private GeoServiceImpl serviceImpl;
	
	@Mock
	private GeoRepository repository;
	
	@Mock
	private GeoFilter filter;
	
	@Mock
	private GeoResponse response;
	
	@Mock
	private GeoRequest request;
	
	@Mock
	private GeoRequest requestPut;
	
	@Mock
	private GeoDomain geoDomain;
	
	@Mock
	PageRequest pageRequest = PageRequest.of(0, 10);
	
	@BeforeAll
	static void beforeAll() {
		
		streetName = "PAIS DE ANDRADE";
		
		number = 234;
		
		neighbourhood = "OLAVO BILAC";
		
		city = "DUQUE DE CAXIAS";
		
		state = "RIO DE JANEIRO";
		
		country = "BRAZIL";
		
		zipcode = "25036240";
		
		latitude = "22.7617399";
		
		longitude = "-43.3186423";
	}
	
	@BeforeEach
	void setup() {
		
		serviceImpl = new GeoServiceImpl(repository);
		
		request = GeoRequest.builder()
				.streetName(streetName)
				.number(number)
				.complement(complement)
				.neighbourhood(neighbourhood)
				.city(city)
				.state(state)
				.country(country)
				.zipcode(zipcode)
				.latitude(latitude)
				.longitude(longitude)
				.build();
	}
	
	@Test
	@DisplayName("Efetua a consulta de endereços com sucesso")
	void getAllAddress() {
		lenient().when(repository.findAll()).thenReturn(List.of(geoDomain));
		
		response = GeoResponse.builder()
				.streetName(streetName)
				.number(number)
				.complement(complement)
				.neighbourhood(neighbourhood)
				.city(city)
				.state(state)
				.country(country)
				.zipcode(zipcode)
				.latitude(latitude)
				.longitude(longitude)
				.build();
		
		lenient().when(geoDomain.toGeoResponse()).thenReturn(response);
		
		List<GeoDomain> listGeoDomain = serviceImpl.findAll(pageRequest);
		
		assertThat(listGeoDomain).asList();
	}
	
	@Test
	@DisplayName("Cadastra um endeço com sucesso")
	void postGeoAddressSucessfully() {
		
		serviceImpl.create(request);
		
		ArgumentCaptor<GeoDomain> captor = ArgumentCaptor.forClass(GeoDomain.class);
		
		verify(repository).save(captor.capture());
		
		assertThat(captor.getValue()).isNotNull();
		
	}
	
	@Test
	@DisplayName("Atualiza um endeço com sucesso")
	void putGeoAddressSucessfully() {
		
		number = 235;
		
		requestPut = GeoRequest.builder()
				.streetName(streetName)
				.number(number)
				.complement(complement)
				.neighbourhood(neighbourhood)
				.city(city)
				.state(state)
				.country(country)
				.zipcode(zipcode)
				.latitude(latitude)
				.longitude(longitude)
				.build();
		
		serviceImpl.create(request);
		
		ArgumentCaptor<GeoDomain> captor = ArgumentCaptor.forClass(GeoDomain.class);
		
		lenient().when(repository.findById(id)).thenReturn(Optional.empty());
		
		BeanUtils.copyProperties(requestPut, request, "id");
		
		verify(repository).save(captor.capture());
		
		assertThat(captor.getValue()).isNotNull();
		
		assertNotEquals(requestPut.getNumber(), geoDomain.getNumber());
		
	}
	
	@Test
	@DisplayName("Consulta endereço cadastrado pelo ID")
	void getGeoAddressByIdSucessfully() {
		
		lenient().when(repository.findById(id)).thenReturn(Optional.of(geoDomain));
		
		response = GeoResponse.builder()
				.id(id)
				.build();
		
		lenient().when(geoDomain.toGeoResponse()).thenReturn(response);
		
		geoDomain = serviceImpl.findById(response.getId());
		
		assertThat(geoDomain.getId()).isNotNull();
		
	}
	
	@Test
	@DisplayName("Excluir um endereço com sucesso")
	void deleteGeoAddressSucessfully() {
		
		lenient().when(repository.findById(id)).thenReturn(Optional.empty());
		
		serviceImpl.delete(id);
		
		assertThat(geoDomain.getId()).isZero();
		
	}

}
